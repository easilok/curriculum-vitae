COMPILER=lualatex
# COMPILER=pdflatex
BIBLIOCOMPILER=biber
CFLAGS=""
DOCNAME="LuisPereira"
MAIN_FILE=cv_7

all : cv_pt cv_en 

cv_pt: $(MAIN_FILE).tex 
	$(COMPILER) $(MAIN_FILE) $(CFLAGS)
	$(COMPILER) $(MAIN_FILE) $(CFLAGS)
	mv $(MAIN_FILE).pdf $(DOCNAME)_cv_PT.pdf

cv_en: $(MAIN_FILE).tex 
	$(COMPILER) "\def\EN{}\input{$(MAIN_FILE)}"
	$(COMPILER) "\def\EN{}\input{$(MAIN_FILE)}"
	mv $(MAIN_FILE).pdf $(DOCNAME)_cv_EN.pdf

.PHONY: clean

clean:
	rm -f *.pdf *.aux *.xml *.bcf *.log *.out
